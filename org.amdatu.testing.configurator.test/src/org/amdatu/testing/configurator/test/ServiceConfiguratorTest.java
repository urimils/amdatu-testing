/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.configurator.test;

import static org.amdatu.testing.configurator.TestConfigurator.*;

import java.util.Dictionary;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import junit.framework.TestCase;

import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.cm.ManagedServiceFactory;

public class ServiceConfiguratorTest extends TestCase {
    private final BundleContext m_context = FrameworkUtil.getBundle(getClass()).getBundleContext();

    /**
     * Tests whether synchronous delivery works as expected.
     */
    public void testSyncConfigurationDeliveryOk() throws Exception {
        String servicePid = "ServiceA";

        String[] ifaces = { ManagedService.class.getName(), Provider.class.getName() };
        Properties props = new Properties();
        props.put(Constants.SERVICE_PID, servicePid);

        Provider service = new ConfigurableProvider();

        ServiceRegistration reg = m_context.registerService(ifaces, service, props);

        configure()
            .add(configuration(servicePid).set("msg", "foo").setSynchronousDelivery(true))
            .applyTo(this);

        // Message should be adapted immediately...
        assertEquals("foo", service.getMessage());

        cleanUp(this);

        // Default value should be set immediately...
        assertEquals("default", service.getMessage());

        reg.unregister();
    }

    /**
     * Tests whether asynchronous delivery works as expected.
     */
    public void testAsyncConfigurationDeliveryOk() throws Exception {
        String servicePid = "ServiceB";

        String[] ifaces = { ManagedService.class.getName(), Provider.class.getName() };
        Properties props = new Properties();
        props.put(Constants.SERVICE_PID, servicePid);

        ConfigurableProvider service = new ConfigurableProvider();

        ServiceRegistration reg = m_context.registerService(ifaces, service, props);

        configure()
            .add(configuration(servicePid).set("msg", "bar").setSynchronousDelivery(false))
            .applyTo(this);

        // We need to wait until the service is actually configured...
        assertTrue(service.m_configured.await(5, TimeUnit.SECONDS));

        assertEquals("bar", service.getMessage());

        cleanUp(this);

        // We need to wait until the service is actually configured...
        assertTrue(service.m_deleted.await(5, TimeUnit.SECONDS));

        assertEquals("default", service.getMessage());

        reg.unregister();
    }

    /**
     * Tests the asynchronous delivery of factory configurations works as expected.
     */
    public void testAsyncFactoryDeliveryOk() throws Exception {
        String servicePid = "ServiceC";

        String[] ifaces = { ManagedServiceFactory.class.getName() };
        Properties props = new Properties();
        props.put(Constants.SERVICE_PID, servicePid);

        ProviderFactory service = new ProviderFactory();

        ServiceRegistration reg = m_context.registerService(ifaces, service, props);

        configure()
            .add(factoryConfiguration(servicePid).set("msg", "qux").setSynchronousDelivery(false))
            .applyTo(this);

        // We need to wait until the service is actually configured...
        assertTrue(service.m_configured.await(5, TimeUnit.SECONDS));

        assertEquals(1, service.m_providers.size());

        cleanUp(this);

        // We need to wait until the service is actually configured...
        assertTrue(service.m_deleted.await(5, TimeUnit.SECONDS));

        assertEquals(0, service.m_providers.size());

        reg.unregister();
    }

    /**
     * Tests the synchronous delivery of factory configurations works as expected.
     */
    public void testSyncFactoryDeliveryOk() throws Exception {
        String servicePid = "ServiceC";

        String[] ifaces = { ManagedServiceFactory.class.getName() };
        Properties props = new Properties();
        props.put(Constants.SERVICE_PID, servicePid);

        ProviderFactory service = new ProviderFactory();

        ServiceRegistration reg = m_context.registerService(ifaces, service, props);

        configure()
            .add(factoryConfiguration(servicePid).set("msg", "bar").setSynchronousDelivery(true))
            .applyTo(this);

        assertEquals(1, service.m_providers.size());

        cleanUp(this);

        assertEquals(0, service.m_providers.size());

        reg.unregister();
    }

    static class ProviderFactory implements ManagedServiceFactory {
        private final CountDownLatch m_configured = new CountDownLatch(1);
        private final CountDownLatch m_deleted = new CountDownLatch(1);
        private final ConcurrentMap<String, Provider> m_providers = new ConcurrentHashMap<>(2);

        @Override
        public String getName() {
            return getClass().getName();
        }

        @Override
        @SuppressWarnings("rawtypes")
        public void updated(String pid, Dictionary properties) throws ConfigurationException {
            String msg = "default";
            if (properties != null) {
                msg = (String) properties.get("msg");
            }

            if (m_providers.putIfAbsent(pid, new ProviderImpl(msg)) == null) {
                m_configured.countDown();
            }
        }

        @Override
        public void deleted(String pid) {
            if (m_providers.remove(pid) != null) {
                m_deleted.countDown();
            }
        }
    }

    /**
     * Configurable implementation of {@link Provider}.
     */
    static class ConfigurableProvider implements Provider, ManagedService {
        private final CountDownLatch m_configured = new CountDownLatch(1);
        private final CountDownLatch m_deleted = new CountDownLatch(2);

        private volatile String m_msg = "default";

        @Override
        @SuppressWarnings("rawtypes")
        public void updated(Dictionary properties) throws ConfigurationException {
            if (properties != null) {
                m_msg = (String) properties.get("msg");

                m_configured.countDown();
            } else {
                m_msg = "default";

                m_deleted.countDown();
            }
        }

        @Override
        public String getMessage() {
            return m_msg;
        }
    }
}