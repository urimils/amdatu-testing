package org.amdatu.testing.configurator;

import static org.amdatu.testing.configurator.TestConfigurator.getContainer;

import org.apache.felix.dm.Component;
import org.apache.felix.dm.Dependency;
import org.apache.felix.dm.DependencyManager;

/**
 * Configuration step that injects a single service to a test case.
 */
public class ServiceDependency implements ConfigurationStep {
	private final Class<?> m_serviceClass;

	private String m_filterString;
	private boolean m_required;
	
	private Dependency m_dependency;

	public ServiceDependency(Class<?> serviceClass) {
	    m_serviceClass = serviceClass;
	    
	    m_required = true; // DO require the service 
	}

    public ServiceDependency setFilter(String filter) {
        m_filterString = filter;
        return this;
    }

    public ServiceDependency setRequired(boolean required) {
        m_required = required;
        return this;
    }

	@Override
	public void apply(Object testCase) {
	    TestConfiguration container = getContainer(testCase);

	    DependencyManager dm = container.getDependencyManager();

	    // @formatter:off
	    m_dependency = dm.createServiceDependency()
	        .setService(m_serviceClass, m_filterString)
	        .setRequired(m_required)
	        .setInstanceBound(true);
	    // @formatter:on

        Component c = container.getComponent();
	    c.add(m_dependency);
	}
	
	@Override
	public void cleanUp(Object testCase) {
        if (m_dependency != null) {
            TestConfiguration container = getContainer(testCase);
            
            Component c = container.getComponent();
            c.remove(m_dependency);
            
            m_dependency = null;
        }
	}
}
