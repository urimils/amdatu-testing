/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.configurator;

import static org.amdatu.testing.configurator.TestConfigurator.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.apache.felix.dm.Component;
import org.apache.felix.dm.ComponentStateListener;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;

public class TestConfiguration {
    private final List<ConfigurationStep> m_steps;

    private DependencyManager m_dm;
    private Component m_component;
    private long m_timeout;

    TestConfiguration() {
        m_steps = new ArrayList<>();
        m_timeout = TimeUnit.SECONDS.toMillis(5L);
    }

    /**
     * Adds a given {@link ConfigurationStep} to this container.
     * 
     * @param step the step to add, cannot be <code>null</code>.
     * @return this container, never <code>null</code>.
     */
    public TestConfiguration add(ConfigurationStep step) {
        Objects.requireNonNull(step, "Step cannot be null!");
        m_steps.add(step);
        return this;
    }

    /**
     * Sets the timeout to wait until all dependencies are satisfied.
     * 
     * @param duration the duration of the timeout;
     * @param unit the unit of time.
     * @return this container, never <code>null</code>.
     */
    public TestConfiguration setTimeout(long duration, TimeUnit unit) {
        m_timeout = unit.toMillis(duration);
        return this;
    }

    /**
     * Applies this configuration to the given test case.
     * @param testCase
     */
    public void applyTo(Object testCase) {
        Objects.requireNonNull(testCase, "TestCase cannot be null!");

        Bundle bundle = FrameworkUtil.getBundle(testCase.getClass());
        Objects.requireNonNull(bundle, "Not running as OSGi test case?!");
        BundleContext context = bundle.getBundleContext();

        m_dm = new DependencyManager(context);
        m_component = m_dm.createComponent().setImplementation(testCase);

        // Make sure the ConfigurationSteps can use this container...
        addContainer(testCase, this);
        
        for (ConfigurationStep step : m_steps) {
            step.apply(testCase);
        }

        final CountDownLatch latch = new CountDownLatch(1);
        m_component.addStateListener(new ComponentStateListener() {
            @Override
            public void stopping(Component c) {
            }

            @Override
            public void stopped(Component c) {
            }

            @Override
            public void starting(Component c) {
            }

            @Override
            public void started(Component c) {
                latch.countDown();
            }
        });

        m_dm.add(m_component);

        try {
            if (!latch.await(m_timeout, TimeUnit.MILLISECONDS)) {
                throw new InjectionFailedException("Failed to configure test case: not all dependencies were satisfied?!");
            }
        } catch (InterruptedException e) {
            throw new InjectionFailedException("Failed to configure test case: interrupted by another thread!", e);
        }
    }

    public void cleanUp(Object testCase) {
        try {
            for (ConfigurationStep step : m_steps) {
                step.cleanUp(testCase);
            }
        } finally {
            m_dm.remove(m_component);
            
            removeContainer(testCase, this);
        }
    }

    final Component getComponent() {
        return m_component;
    }

    final DependencyManager getDependencyManager() {
        return m_dm;
    }
}
