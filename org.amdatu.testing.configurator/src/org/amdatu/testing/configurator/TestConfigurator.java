/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.configurator;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Utilities for helping out with common OSGi integration test operations such as
 * configuring and retrieving service instances.
 * 
 * Usage:
 * 
 * <ul>
 * <li> Make sure your test case inherits from JUnit's <code>TestCase</code> class.</li>
 * <li> Import the public methods defined in this class statically to your test case </li>
 * <li> Define a private volatile field for each service you want to inject to your test </li>
 * <li> Override the setUp() method, and do the necessary configurations. </li>
 * <li> Override the tearDown() method and call the static cleanUpTest() method. 
 * This will revert any persistent changes done by any of the configuration steps. </li>
 * </ul>
 * Here is an example of configuring a service called MyService that uses the amdatu mongodb service:
 *   
 * </ul>  
 *  <pre>
 *  <code>
 *  import static org.amdatu.testing.configurator.TestConfigurator.*;
 *  // other necessary imports 
 *  
 *  public class MyTestCase extends TestCase {
 *  	private volatile MyService myService;
 *  
 *  	public void setUp() {
 *  		configureTest(this,
 *  			configureFactory("org.amdatu.mongo").set("dbName", "test"),
 *  			inject(MyService.class));				
 *  	}
 *  
 *  	public void tearDown() {
 *  		cleanUpTest(this);
 *  	}
 *  
 *  	public void testSomething() {
 *  		assertNotNull(myService);
 *  	} 
 *  }
 *  </code>
 *  </pre>
 */
public final class TestConfigurator {
    private static ConcurrentMap<Integer, TestConfiguration> IDX = new ConcurrentHashMap<>();

    private TestConfigurator() {
        // Avoid instances being made...
    }

	/**
	 * Sets up a JUnit test case, performing all specified configurations and registering
	 * all necessary services.
	 * 
	 * @param testCase The test case to set up.
	 * @param steps List of configuration steps.
	 */
    @Deprecated
	public static void configureTest(Object testCase, ConfigurationStep ... steps) {
	    TestConfiguration container = new TestConfiguration();
	    for (ConfigurationStep step : steps) {
	        container.add(step);
	    }
	    container.applyTo(testCase);
	}

    /**
     * Sets up a JUnit test case, performing all specified configurations and registering
     * all necessary services.
     * 
     * @return the container of configuration steps.
     */
    public static TestConfiguration configure() {
        return new TestConfiguration();
    }

	/**
	 * @param testCase the test case to clean up, cannot be <code>null</code>.
	 */
	public static void cleanUp(Object testCase) {
        getContainer(testCase).cleanUp(testCase);
	}

	static TestConfiguration addContainer(Object testCase, TestConfiguration container) {
        if (IDX.putIfAbsent(System.identityHashCode(testCase), container) != null) {
            throw new RuntimeException(String.format("Test (%1$s) already configured?!", testCase));
        }
        return container;
	}
	
	static TestConfiguration getContainer(Object testCase) {
	    TestConfiguration result = IDX.get(System.identityHashCode(testCase));
	    Objects.requireNonNull(result, "No test-step container found for " + testCase + "; is it configured properly?!");
        return result;
	}

    static TestConfiguration removeContainer(Object testCase, TestConfiguration container) {
        if (!IDX.remove(System.identityHashCode(testCase), container)) {
            throw new RuntimeException(String.format("Failed to clean up test (%1$s)?!", testCase));
        }
        return container;
    }

	/**
	 * Configures the specified service factory.
	 * 
	 * @param pid The pid of the service factory to configure
	 * @return ServiceConfigurator instance used to set configuration properties.
	 */
	public static ServiceConfigurator factoryConfiguration(String pid) {
        return ServiceConfigurator.createFactoryServiceConfigurator(pid);
	}

	/**
	 * Configures a service with the specified pid. 
	 * 
	 * @param pid The pid of the service to configure.
	 * @return ServiceConfigurator instance used to set configuration properties.
	 */
	public static ServiceConfigurator configuration(String pid) {
		return ServiceConfigurator.createServiceConfigurator(pid);
	}

	/**
	 * Injects a service dependency.
     * @param serviceClass The class of the service to inject.
	 * @return a {@link ServiceDependency} builder allowing you to customize the injection further.
	 */
	public static ServiceDependency serviceDependency(Class<?> serviceClass) {
		return new ServiceDependency(serviceClass);
	}
}
