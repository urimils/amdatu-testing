package org.amdatu.testing.mongo;

/**
 * MongoDB configuration steps for the OSGiTestConfigurator.
 */
public class OSGiMongoTestConfigurator {
	
	private OSGiMongoTestConfigurator() {
		
	}
	
	public static MongoConfigurationStep configureMongoDb() {
		MongoConfigurationStep step = new MongoConfigurationStep();
		return step;
	}
	
}
